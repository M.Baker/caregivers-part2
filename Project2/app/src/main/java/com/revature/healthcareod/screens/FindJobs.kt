/*
Healthcare OnDemand
Anderson Adams
Find Jobs Class
Class will create/display job finder recycle adapter
 */

package com.revature.healthcareod.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.gms.maps.model.LatLng
import com.revature.healthcareod.R
import com.revature.healthcareod.databinding.FragmentFindJobsBinding
import com.revature.healthcareod.datahandlers.*
import com.revature.healthcareod.viewmodels.MainViewModel

class FindJobs : Fragment() {

    private val viewModel:MainViewModel by activityViewModels()

    private var _binding: FragmentFindJobsBinding?=null
    private val binding get()=_binding!!
    lateinit var userName:String
    private var userRef: NurseAccount? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        println("The view is being created")
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        _binding= FragmentFindJobsBinding.inflate(inflater,container,false)
        val view=binding.root
        //bind adapter with all open jobs list
        binding.rvFindJobs.adapter = JobFinder(viewModel.allOpenJobs,viewModel)
//        binding.btnFjBack.setOnClickListener { viewModel.navigateToJobSearcher() }

        viewModel.navigateToJobSearcher.observe(viewLifecycleOwner, Observer<Boolean> { status ->
            if (status) {
                findNavController().navigate(R.id.action_findJobs_to_jobSearcher)
                viewModel.onNavigatedToJobSearcher()
            }
        })
        viewModel.jobFinderU.observe(viewLifecycleOwner,Observer<Boolean>{status->
            if (status) {
                binding.rvFindJobs.adapter = JobFinder(viewModel.allOpenJobs, viewModel)
                viewModel.onJobFinderURead()
            }
        })

        viewModel.locationChanged.observe(viewLifecycleOwner,Observer<Boolean>{status->
            if(status) {
                findNavController().navigate(R.id.action_findJobs_to_maps)
                viewModel.closeMap()
            }
        })

        viewModel.acceptJobToast.observe(viewLifecycleOwner,Observer<Boolean>{status->
            if(status) {
                Toast.makeText(requireContext(), "Could not accept job", Toast.LENGTH_SHORT)
            }
        })


        // binding.btnBack.setOnClickListener { back() }
        //binding.btnSaveProfile.setOnClickListener { submit() }
        //Inflate the layout for this fragment
        return view
    }



}