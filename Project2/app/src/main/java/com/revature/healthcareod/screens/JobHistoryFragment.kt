package com.revature.healthcareod.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.revature.healthcareod.R
import com.revature.healthcareod.databinding.FragmentJobHistoryBinding
import com.revature.healthcareod.datahandlers.*
import com.revature.healthcareod.screens.JobHistoryFragmentDirections
import com.revature.healthcareod.viewmodels.MainViewModel

// Job History by Matt

class JobHistoryFragment : Fragment() {

    private val viewModel:MainViewModel by activityViewModels()

    private var _binding: FragmentJobHistoryBinding?=null
    private val binding get()=_binding!!
    lateinit var userName:String
    private var userRef: NurseAccount? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        _binding= FragmentJobHistoryBinding.inflate(inflater,container,false)
        val view=binding.root
        binding.rvJobHistory.adapter= JobAdapter(viewModel.jobHistory)
        binding.btnJhBack.setOnClickListener { viewModel.navigateToJobSearcher() }

        binding.jhTotalAmountTv.text=viewModel.earnings
        viewModel.navigateToJobSearcher.observe(viewLifecycleOwner, Observer<Boolean> { status ->
            if (status) {
                findNavController().navigate(R.id.action_jobHistoryFragment_to_jobSearcher)
                viewModel.onNavigatedToJobSearcher()
            }
        })


        // binding.btnBack.setOnClickListener { back() }
        //binding.btnSaveProfile.setOnClickListener { submit() }
        //Inflate the layout for this fragment
        return view
    }
    //navigate back
}