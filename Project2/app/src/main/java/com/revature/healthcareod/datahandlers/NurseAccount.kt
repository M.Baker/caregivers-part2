/*
Healthcare OnDemand
Anderson Adams
Nurse Account Class
Class will be object reference to nurse account that will hold names, ids, and job history
 */

package com.revature.healthcareod.datahandlers

import android.util.Log
import java.io.Serializable
import java.util.*
import kotlin.collections.ArrayList

class NurseAccount(
    var firstName: String,
    var lastName: String,
    val userName: String,
    var password: String,
    var phone: String,
    var address: String
) : Serializable {
    //nurse account info, made serializable to be sent through intents.

    val education = ArrayList<Degree>()
    //add degree info here

    //job history

    private var verified: Boolean = false

    fun getJobHistory(): List<Job> {
        return DummyData.dummyJH.filter { it.status == "Completed" || it.status == "Declined" }
            .filter { it.userName == userName } as ArrayList<Job>
    }//will return all jobs that customer has accepted

    fun getAllOpenJobs(): ArrayList<Job> {
        //check if jobs ended and change
        val currentDate = Date(System.currentTimeMillis())
        val cal: Calendar = Calendar.getInstance()
        cal.time = currentDate
        var day: Int; var month: Int; var year: Int

       for (e in DummyData.dummyJH.filter { it.status == "Open" } as ArrayList<Job>) {
           println("address get all open jobs ${e.address} and job name ${e.jobName}"  )
           if(e.endDate != null) {

               month = e.endDate!!.split("/")[0].toInt()
               day = e.endDate!!.split("/")[1].toInt()
               year = e.endDate!!.split("/")[2].toInt()

           //    println("end day, $day, $month, $year")
         //     println("current day, ${cal.get(Calendar.DAY_OF_MONTH)}, ${cal.get(Calendar.MONTH)}, ${cal.get(Calendar.YEAR)} ")

               if(year < cal.get(Calendar.YEAR)){ e.status = "Completed"}
                else if (month < cal.get(Calendar.MONTH) && (year <= cal.get(Calendar.YEAR))){e.status = "Completed"}
                   else if (day < cal.get(Calendar.DAY_OF_MONTH) && (month <= cal.get(Calendar.MONTH)) && (year <= cal.get(Calendar.YEAR))){
                    e.status = "Completed"

               }//check that the current date is past, if so complete job

           }//if the job has an end date

        }//for all jobs
        return DummyData.dummyJH.filter { it.status == "Open" } as ArrayList<Job>
    }//will return all jobs in dummy list of jobs

    fun getAllAcceptedJobs(): ArrayList<Job> {
        //check if jobs ended and change
        val currentDate = Date(System.currentTimeMillis())
        val cal: Calendar = Calendar.getInstance()
        cal.time = currentDate
        var day: Int; var month: Int; var year: Int


        for (e in DummyData.dummyJH.filter { it.userName == userName }
            .filter { it.status == "Accepted" } as ArrayList<Job>) {
         //   println("${e.endDate}")
            if(e.endDate != null) {
                month = e.endDate!!.split("/")[0].toInt()
                day = e.endDate!!.split("/")[1].toInt()
                year = e.endDate!!.split("/")[2].toInt()

             //   println("end day, $day, $month, $year")
             //   println("current day, ${cal.get(Calendar.DAY_OF_MONTH)}, ${cal.get(Calendar.MONTH)}, ${cal.get(Calendar.YEAR)} ")

                if(year < cal.get(Calendar.YEAR)){ e.status = "Completed"}
                else if (month < cal.get(Calendar.MONTH) && (year <= cal.get(Calendar.YEAR))){e.status = "Completed"}
                else if (day < cal.get(Calendar.DAY_OF_MONTH) && (month <= cal.get(Calendar.MONTH)) && (year <= cal.get(Calendar.YEAR))){
                    e.status = "Completed"

                }//check that the current date is past, if so complete job

            }//if the job has an end date
        }
        Log.i("foo", "testforloopcompletion")

        return DummyData.dummyJH.filter { it.userName == userName }
            .filter { it.status == "Accepted" } as ArrayList<Job> //it.userName == userName } as ArrayList<Job>
        //listing all accepted job because user names are not being set 

    }//returns list of all josb that customer has accepted, differs from job history as jobs can be rejected here and does not include duplicates

    fun addJob(job: Job) {
        //jobHistory.add(job)
    }//adds job to job history, does include duplicates

    fun getJobDayOfWeek(dayOfWeek: Int, dayClicked: Int, monthClicked: Int, yearClicked: Int ): Job? {
        var day: Int; var month: Int; var year: Int
        var dayEnd: Int; var monthEnd: Int; var yearEnd: Int

        val currentJobs = getAllAcceptedJobs()
        for (e in currentJobs) {
            month = e.startDate!!.split("/")[0].toInt()
            day = e.startDate!!.split("/")[1].toInt()
            year = e.startDate!!.split("/")[2].toInt()

            monthEnd = e.endDate!!.split("/")[0].toInt()
            dayEnd = e.endDate!!.split("/")[1].toInt()
            yearEnd = e.endDate!!.split("/")[2].toInt()

            //println("clicked date" + dayClicked + monthClicked + yearClicked)
            //println("job start date" + day + month + year)
            //println("job start date" + dayEnd + monthEnd + yearEnd)


            if ((dayClicked >= day && monthClicked >= month && yearClicked >= year) || (monthClicked > month && yearClicked >= year) || (yearClicked > year)){
               if((dayClicked < dayEnd && monthClicked <= monthEnd && yearClicked <= yearEnd) || (monthClicked < monthEnd && yearClicked <= yearEnd) || (yearClicked < yearEnd)) {
                 //  println(e.weeklySchedule)
                 //  println(dayOfWeek)
                   if (e.weeklySchedule[dayOfWeek - 1] == '1') {
                       println("found job $e")
                       return e
                   }
               }
            }//check that the current date is past, if so complete job
        }
        println("returning null from get job day of week")
        return null
    }

    fun checkJobAvalible(job: Job): Boolean {
        //check to see what days are being returned, can think about making weekly schedule getter helper
        println("job ${job.weeklySchedule}")
        val currentJobs = getAllAcceptedJobs()
        var counter = 0
        for (e in currentJobs) {

            //println(e.weeklySchedule + " " + e.jobName)

            counter = 0
            for (i in e.weeklySchedule) {

                //println("counter $counter")
               // println("job ${job.weeklySchedule[counter]}")

                if (i == '1' && job.weeklySchedule[counter] == '1') {
                    return false
                }
                counter++
            }
        }
        return true
    }


    fun view_Account() {
        println("Account Id: $userName")
        println("Password: $password")
        println("First Name: $firstName")
        println("Last Name: $lastName")
        println("verified: $verified")
    }//debug meathod for printing

    fun verify() {
        verified = true
    }//verify account

    fun get_verify(): Boolean {
        return verified
    }//get the account verification

    fun declineJob(job: Job): Pair<Job, Job> {
        job.status = Status.Declined.name
        var job2 = Job()
        job2.hospitalId = job.hospitalId
        job2.userName = ""
        job2.ratePerHour = job.ratePerHour
        job2.totalAmount = job.totalAmount
        job2.jobName = job.jobName
        job2.jobDescription = job.jobDescription
        job2.startDate = job.startDate
        job2.endDate = job.endDate
        job2.status = "Open"
        DummyData.dummyJH.add(job2)
        return Pair(job, job2)
    }

    fun acceptJob(job: Job): Job {
        job.userName = userName
        job.status = "Accepted"

        val currentDate = Date(System.currentTimeMillis())
        val cal: Calendar = Calendar.getInstance()
        cal.time = currentDate
        job.startDate = "${cal.get(Calendar.MONTH) + 1}/${cal.get(Calendar.DAY_OF_MONTH)}/${cal.get(Calendar.YEAR)}"
        return job
    }

    fun completeJob(job: Job): Pair<Job, Job> {
        job.status = "Completed"

        var job2 = Job()
        job2.hospitalId = job.hospitalId
        job2.userName = ""
        job2.ratePerHour = job.ratePerHour
        job2.totalAmount = job.totalAmount
        job2.jobName = job.jobName
        job2.jobDescription = job.jobDescription
        job2.startDate = job.startDate
        job2.endDate = job.endDate
        job2.status = "Open"
        DummyData.dummyJH.add(job2)
        return Pair(job, job2)
    }
}