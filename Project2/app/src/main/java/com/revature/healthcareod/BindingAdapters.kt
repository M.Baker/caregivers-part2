package com.revature.healthcareod

import android.widget.ImageView
import android.widget.TextView
import androidx.core.net.toUri
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.revature.healthcareod.datahandlers.Degree
import com.revature.healthcareod.datahandlers.EduAdapter

@BindingAdapter("school")
fun bindText(tv: TextView, s:String?){
    s?.let{
        tv.text=s
    }
}

@BindingAdapter("major")
fun bindText2(tv: TextView, s:String?){
    s?.let{
        tv.text=s
    }
}

@BindingAdapter("level")
fun bindText3(tv: TextView, s:String?){
    s?.let{
        tv.text=s
    }
}

@BindingAdapter("gradDate")
fun bindText4(tv: TextView, s:String?){
    s?.let{
        tv.text=s
    }
}

@BindingAdapter("getHosNameBId")
fun bindText5(tv: TextView, s:String?){
    s?.let{
        tv.text=s.toString()
    }
}

@BindingAdapter("jobName")
fun bindText6(tv: TextView, s:String?){
    s?.let{
        tv.text=s
    }
}

@BindingAdapter("jobDescription")
fun bindText7(tv: TextView, s:String?){
    s?.let{
        tv.text=s
    }
}

@BindingAdapter("ratePerHour")
fun bindText8(tv: TextView, s:String?){
    s?.let{
        tv.text=s.toString()
    }
}

@BindingAdapter("totalAmount")
fun bindText9(tv: TextView, s:String?){
    s?.let{
        tv.text=s.toString()
    }
}
@BindingAdapter("status")
fun bindText10(tv: TextView, s:String?){
    s?.let{
        tv.text=s.toString()
    }
}
@BindingAdapter("stringST")
fun bindText11(tv: TextView, s:String?){
    s?.let{
        tv.text=s.toString()
    }
}
@BindingAdapter("stringET")
fun bindText12(tv: TextView, s:String?){
    s?.let{
        tv.text=s.toString()
    }
}
@BindingAdapter("imageUrl")
fun bindImage(imgView:ImageView,imgUrl:String?){
    imgUrl?.let{
        var imgUri=imgUrl.toUri().buildUpon().scheme("https").build()
        Glide.with(imgView.context).load(imgUri).apply(RequestOptions().placeholder(R.drawable.loading_animation).error(R.drawable.ic_broken_image)).into(imgView)
    }
}

//@BindingAdapter("eduList")
//fun bindRecyclerView(recyclerView: RecyclerView, data:ArrayList<Degree>?){
//    val adapter=recyclerView.adapter as EduAdapter
//    adapter.submitlist(data?:ArrayList())
//}