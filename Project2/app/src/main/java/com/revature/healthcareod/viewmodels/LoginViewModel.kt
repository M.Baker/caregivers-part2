package com.revature.healthcareod.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.revature.healthcareod.database.nursedatafiles.NurseData
import com.revature.healthcareod.database.nursedatafiles.NurseDataRepository
import com.revature.healthcareod.datahandlers.Users
import kotlinx.coroutines.launch

class LoginViewModel : ViewModel() {
    private var _navigateToFirstFragment = MutableLiveData<Boolean>(false)
    private val _result = MutableLiveData<String>("")

    var nurseDataRepository: NurseDataRepository? = null

    val result: LiveData<String>
        get() = _result
    val navigateToFirstFragment: LiveData<Boolean>
        get() = _navigateToFirstFragment
    private val _toastString = MutableLiveData<String>()
    val toastString: LiveData<String>
        get() = _toastString

    fun navigateToFirstFragment() {
        _navigateToFirstFragment.value = true
    }

    fun onNavigatedToFirstFragment() {
        _navigateToFirstFragment.value = false
    }

    fun register(firstName: String, lastName: String, userName: String, password: String, phone: String, address: String) {

        val result = Users.register(firstName, lastName, userName, password, phone, address)
        _toastString.value=when (result) {
            "taken"-> "Account already registered"
            "namef empty"-> "Incorrect First name"
            "namel empty"-> "Incorrect Last name"
            "username empty"->"Incorrect User name"
            "passwordempty"->"Incorrect password"
            "registered"->{
                val nurseData = NurseData(firstName, lastName, userName, password, phone, address)
                insertNurseData(nurseData)
                "Account registered, proceeded to login.".also{navigateToFirstFragment()}
            }
            else->"Severe Error"
        }

    }

    fun insertNurseData(nurseData: NurseData) = viewModelScope.launch {
        nurseDataRepository?.insert(nurseData)
    }


}