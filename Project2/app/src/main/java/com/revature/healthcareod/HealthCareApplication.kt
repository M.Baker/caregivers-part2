package com.revature.healthcareod

import android.app.Application
import com.revature.healthcareod.database.HealthCareDatabase
import com.revature.healthcareod.database.jobdatafiles.JobDataRepository
import com.revature.healthcareod.database.nursedatafiles.NurseDataRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob

class HealthCareApplication : Application() {
    // No need to cancel this scope as it'll be torn down with the process
    val applicationScope = CoroutineScope(SupervisorJob())

    // Using by lazy so the database and the repository are only created when they're needed
    // rather than when the application starts
    val database by lazy { HealthCareDatabase.getDatabase(this, applicationScope) }
    val nurseDataRepository by lazy { NurseDataRepository(database.nurseDataDao()) }
    val jobDataRepository by lazy {JobDataRepository(database.jobDataDao())}
}