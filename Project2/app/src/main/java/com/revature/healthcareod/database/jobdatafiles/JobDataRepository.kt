package com.revature.healthcareod.database.jobdatafiles

import androidx.annotation.WorkerThread
import kotlinx.coroutines.flow.Flow

class JobDataRepository(private val jobDataDao: JobDataDao){

    val allJobsData: Flow<List<JobData>> = jobDataDao.getAllJobsData()


    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(jobData: JobData) {
        jobDataDao.insert(jobData)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun update(jobData: JobData) {
        jobDataDao.updateJobData(jobData)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun getNumOfJobsData(): Int {
        return jobDataDao.getCount()
    }

}






