package com.revature.healthcareod.database.jobdatafiles

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.android.gms.maps.model.LatLng


@Entity(tableName = "jobDataTable")
data class JobData(

        @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "jobId") val jobId: Int,
        @ColumnInfo(name = "hospitalId") val hospitalId: Int,
        @ColumnInfo(name = "userName") val userName: String,
        @ColumnInfo(name = "ratePerHour") val ratePerHour: Int,
        @ColumnInfo(name = "totalAmount") val totalAmount: Int,
        @ColumnInfo(name = "jobName") val jobName: String,
        @ColumnInfo(name = "jobDescription") val jobDescription: String,
        @ColumnInfo(name = "startDate") val startDate: String,
        @ColumnInfo(name = "endDate") val endDate: String,
        @ColumnInfo(name = "status") val status: String,
        @ColumnInfo(name = "schedule") val schedule: String,
        @ColumnInfo(name = "address") val address: String,
        @ColumnInfo(name = "textAddress") val textAddress: String

)
