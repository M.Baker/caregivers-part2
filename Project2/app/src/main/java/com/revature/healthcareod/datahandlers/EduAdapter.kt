package com.revature.healthcareod.datahandlers

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.revature.healthcareod.R
import com.revature.healthcareod.databinding.EduElementBinding

class EduAdapter(var eduList:ArrayList<Degree>) : RecyclerView.Adapter<EduAdapter.ItemViewHolder>() {

//    var eduList:List<Degree>
//        set(l:List<Degree>){
//        _eduList=ArrayList(l)
//    }
//        get()=_eduList


    class ItemViewHolder(private var binding: EduElementBinding):RecyclerView.ViewHolder(binding.root){
        fun bind(deg:Degree){
            binding.degree=deg
            binding.executePendingBindings()
        }

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val adapterLayout= LayoutInflater.from(parent.context).inflate(R.layout.edu_element,parent,false)
        return  ItemViewHolder(EduElementBinding.inflate(LayoutInflater.from(parent.context)))
    }


    override fun getItemCount(): Int {
        return eduList.size
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val value=eduList[position]
        holder.bind(value)
    }

}
