package com.revature.healthcareod.screens

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.revature.healthcareod.R
import com.revature.healthcareod.databinding.FragmentJobSearcherBinding
import com.revature.healthcareod.viewmodels.MainViewModel

// Landing page by Matt
/**
 * A simple [Fragment] subclass.
 * Use the [JobSearcher.newInstance] factory method to
 * create an instance of this fragment.
 */
class JobSearcher : Fragment() {
    private val viewModel:MainViewModel by activityViewModels()

    private var _binding: FragmentJobSearcherBinding?=null
    private val bin get()=_binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding= FragmentJobSearcherBinding.inflate(inflater,container,false)
        val view=bin.root
        bin.btnLogout.setOnClickListener { viewModel.navigateToFirstFragment() }
        bin.btnJsViewAccount.setOnClickListener { viewModel.navigateToAccountview() }
        bin.btnJsCurrentJobs.setOnClickListener { viewModel.navigateToCurrentJobs() }
        bin.btnJsFindJobs.setOnClickListener { viewModel.navigateToFindJobs() }
        bin.btnJsSchedule.setOnClickListener { viewModel.navigateToSchedule()}
        bin.btnJsJobHistory.setOnClickListener { viewModel.navigateToJobHistory() }
        //check for not being logged in

        if ((viewModel as MainViewModel).user==null){
            viewModel.navigateToFirstFragment()
        }

        viewModel.navigateToFirstFragment.observe(viewLifecycleOwner, Observer<Boolean> { status ->
            if (status) {
                findNavController().navigate(R.id.action_jobSearcher_to_FirstFragment)
                viewModel.onNavigatedToFirstFragment()
            }
        })
        viewModel.navigateToAccountview.observe(viewLifecycleOwner, Observer<Boolean> { status ->
            if (status) {
                findNavController().navigate(R.id.action_jobSearcher_to_accountViewFragment)
                viewModel.onNavigatedAccountview()
            }
        })
        viewModel.navigateToCurrentJobs.observe(viewLifecycleOwner, Observer<Boolean> { status ->
            if (status) {
                findNavController().navigate(R.id.action_jobSearcher_to_currentJobs)
                viewModel.onNavigatedCurrentJobs()
            }
        })
        viewModel.navigateToFindJobs.observe(viewLifecycleOwner, Observer<Boolean> { status ->
            if (status) {
                findNavController().navigate(R.id.action_jobSearcher_to_findJobs)
                viewModel.onNavigatedToFindJobs()
            }
        })
        viewModel.navigateToSchedule.observe(viewLifecycleOwner, Observer<Boolean> { status ->
            if (status) {
                findNavController().navigate(R.id.action_jobSearcher_to_schedulefragment)
                viewModel.onNavigatedToSchedule()
            }
        })
        viewModel.navigateToJobHistory.observe(viewLifecycleOwner, Observer<Boolean> { status ->
            if (status) {
                findNavController().navigate(R.id.action_jobSearcher_to_jobHistoryFragment)
                viewModel.onNavigatedToJobHistory()
            }
        })

        //Inflate the layout for this fragment
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        bin.textViewHeader.text=getString(R.string.logged_in_string,viewModel.user?.userName?:"null")

        super.onViewCreated(view, savedInstanceState)
    }
    override fun onStart(){
        super.onStart()
    }

    override fun onResume() {
        super.onResume()
    }

}