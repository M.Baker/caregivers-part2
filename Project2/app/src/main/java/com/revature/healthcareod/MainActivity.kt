package com.revature.healthcareod

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.revature.healthcareod.database.jobdatafiles.JobData
import com.revature.healthcareod.database.jobdatafiles.JobDataRepository
import com.revature.healthcareod.database.nursedatafiles.NurseData
import com.revature.healthcareod.database.nursedatafiles.NurseDataRepository
import com.revature.healthcareod.datahandlers.DummyData
import com.revature.healthcareod.datahandlers.Users
import com.revature.healthcareod.viewmodels.MainViewModel


class MainActivity : AppCompatActivity() {
    //private val tempUser = Users()




    override fun onCreate(savedInstanceState: Bundle?) {

       // tempUser.readUsers(this)//get the users list from data

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))

        val mainViewModel = ViewModelProvider(this).get(MainViewModel::class.java)


        mainViewModel.nurseDataRepository = getNursesDataRepository()
        mainViewModel.initNursesData()
        //========================================================================================
        val nursesDataObserver = Observer<List<NurseData>> { nursesData ->
            if (mainViewModel.nurseDataIsReaded) {
                return@Observer
            } else {
                mainViewModel.nurseDataIsReaded = true
                Users.fillAllNurses(nursesData)
            }
        }
        // Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
        mainViewModel.nursesDataLiveData?.observe(this, nursesDataObserver)
        //========================================================================================


        mainViewModel.jobDataRepository = getJobsDataRepository()
        mainViewModel.createJobsDataLiveData()
        //========================================================================================
        val jobsDataObserver = Observer<List<JobData>> { jobsData ->
            Log.d("JOBSDB", "Jobs data used with number of ${jobsData.size} elements")
            if (mainViewModel.jobDataIsReaded || jobsData.size < DummyData.getDummyJobs().size) {
                return@Observer
            } else {
                mainViewModel.jobDataIsReaded = true
                DummyData.fillAllJobs(jobsData)
            }
        }
        // Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
        mainViewModel.jobsDataLiveData?.observe(this, jobsDataObserver)
        //========================================================================================

        mainViewModel.initJobRows()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> logout()
            R.id.action_back -> {onBackPressed(); return true}
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onStop() {
        //tempUser.writeUsers() //write the users list when app is turned off
        super.onStop()
    }



    fun getNursesDataRepository(): NurseDataRepository {
            val myApplication: HealthCareApplication = application as HealthCareApplication
            return myApplication.nurseDataRepository
    }

    fun getJobsDataRepository(): JobDataRepository {
        val myApplication: HealthCareApplication = application as HealthCareApplication
        return myApplication.jobDataRepository
    }


    fun logout(): Boolean {
        val intent = intent
        finish()
        startActivity(intent)
        return true
    }

}