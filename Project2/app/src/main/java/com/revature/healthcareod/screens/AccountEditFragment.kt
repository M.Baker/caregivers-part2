package com.revature.healthcareod.screens

import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.revature.healthcareod.R
import com.revature.healthcareod.databinding.FragmentAccountEditBinding
import com.revature.healthcareod.datahandlers.NurseAccount
import com.revature.healthcareod.viewmodels.MainViewModel

class AccountEditFragment : Fragment() {
    private val viewModel: MainViewModel by activityViewModels()

    private var _binding: FragmentAccountEditBinding? = null
    private val binding get() = _binding!!
    lateinit var userName: String
    private var userRef: NurseAccount? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //Load the user

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentAccountEditBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        val view = binding.root
        binding.viewModel=viewModel
        //binding.accViewRecV.layoutManager=CustomLayoutManager(requireContext())
        //binding.accViewRecV.adapter= EduAdapter(viewModel.eduList)
//        binding.btnBack.setOnClickListener { viewModel.navigateToAccountview() }
        binding.btnSaveProfile.setOnClickListener {
            viewModel.submit(
                binding.editTextAccViewFName.text.toString(),
                binding.editTextAccViewLName.text.toString(),
                binding.editTextAccViewAdrs.text.toString()
            )
        }
        binding.btnSavePassword.setOnClickListener {
            viewModel.savePass(
                binding.editTextPassword.text.toString(),
                binding.editTextTextConfirmPassword.text.toString()
            )
        }
        binding.btnSubmitEdu.setOnClickListener {
            Toast.makeText(
                this.requireContext(), when (viewModel.saveEdu(
                    binding.accViewSchoolEt.text.toString(),
                    binding.accViewGradDat.text.toString(),
                    binding.accViewLevel.text.toString(),
                    binding.accViewMajorEt.text.toString()
                )) {
                    0 -> getString(R.string.edu_edited)
                    1 -> getString(R.string.edu_invalid)
                    else -> "Severe Error"
                }, Toast.LENGTH_LONG
            ).show()
            binding.accViewSchoolEt.setText("")
            binding.accViewGradDat.setText("")
            binding.accViewLevel.setText("")
            binding.accViewMajorEt.setText("")
        }
        binding.editTextAccViewFName.text = SpannableStringBuilder(viewModel.firstName)
        binding.editTextAccViewLName.text = SpannableStringBuilder(viewModel.lastName)
        binding.editTextAccViewAdrs.text = SpannableStringBuilder(viewModel.address)

        //Inflate the layout for this fragment
//        viewModel.edu.observe(viewLifecycleOwner, Observer<Boolean> { status ->
//            if (status) {
//                Log.e("EDU","test")
//                binding.accViewRecV.adapter= EduAdapter(viewModel.eduList)
//                viewModel.onEduUpdated()
//                binding.accViewMajorEt.text=viewModel.blankedit
//                binding.accViewSchoolEt.text=viewModel.blankedit
//                binding.accViewGradDat.text=viewModel.blankedit
//                binding.accViewLevel.text=viewModel.blankedit
//
//            }
//        })

        viewModel.navigateToAccountview.observe(viewLifecycleOwner, Observer<Boolean> { status ->
            if (status) {
                findNavController().navigate(R.id.action_accountEditFragment_to_accountViewFragment)
                viewModel.onNavigatedAccountview()
            }
        })
        viewModel.passC.observe(viewLifecycleOwner, Observer<Int> { status ->
            if (status != 0) {
                Toast.makeText(this.requireContext(), when (status) {
                    1 -> getString(R.string.success_password).also { viewModel.onPassCRead() }
                    2 -> getString(R.string.error_message_unmatched).also { viewModel.onPassCRead() }
                    3 -> getString(R.string.blank_password).also { viewModel.onPassCRead() }
                    else -> "Severe Error"
                }, Toast.LENGTH_SHORT).show()
            }
        })
        viewModel.pU.observe(viewLifecycleOwner, Observer<Int> { status ->
            if (status != 0) {
                Toast.makeText(this.requireContext(), when (status) {
                    1 -> getString(R.string.success_profile).also {
                        binding.editTextAccViewFName.text =
                            SpannableStringBuilder(viewModel.firstName)
                        binding.editTextAccViewLName.text =
                            SpannableStringBuilder(viewModel.lastName)
                        binding.editTextAccViewAdrs.text = SpannableStringBuilder(viewModel.address)
                        viewModel.onPURead()
                    }
                    2 -> getString(R.string.error_message_nochanges).also { viewModel.onPURead() }
                    else -> "Severe Error"
                }, Toast.LENGTH_SHORT).show()
            }
        })


        return view
    }
}