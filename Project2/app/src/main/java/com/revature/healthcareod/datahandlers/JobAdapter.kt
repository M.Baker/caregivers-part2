package com.revature.healthcareod.datahandlers

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.revature.healthcareod.R
import com.revature.healthcareod.databinding.EduElementBinding
import com.revature.healthcareod.databinding.JobElementBinding

class JobAdapter(private val dataset:List<Job>):RecyclerView.Adapter<JobAdapter.ItemViewHolder>() {
    class ItemViewHolder(private var binding: JobElementBinding):RecyclerView.ViewHolder(binding.root){
        fun bind(job: Job){
            binding.job=job
            binding.executePendingBindings()
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val adapterLayout= LayoutInflater.from(parent.context).inflate(R.layout.job_element,parent,false)
        return ItemViewHolder(JobElementBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val job=dataset[position]
        holder.bind(job)
    }

    override fun getItemCount(): Int {
        return dataset.size
    }

}