package com.revature.healthcareod.database.jobdatafiles

import androidx.room.*
import kotlinx.coroutines.flow.Flow


@Dao
interface JobDataDao {

    @Query("SELECT * FROM jobDataTable")
    fun getAllJobsData(): Flow<List<JobData>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(jobData: JobData)

    @Query("DELETE FROM jobDataTable")
    suspend fun deleteAll()

    @Update
    suspend fun updateJobData(jobData: JobData?): Int

    @Query("SELECT COUNT(jobId) FROM jobDataTable")
    suspend fun getCount(): Int

}