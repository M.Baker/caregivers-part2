/*
Healthcare OnDemand
Anderson Adams
Login UI Class
Class Login UI will push to registration or check if user can login
 */

package com.revature.healthcareod.screens

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.activity.ComponentActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.revature.healthcareod.*
import com.revature.healthcareod.datahandlers.NurseAccount
import com.revature.healthcareod.datahandlers.Users
import com.revature.healthcareod.viewmodels.MainViewModel


/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class FirstFragment : Fragment() {

    private val viewModel:MainViewModel by activityViewModels()

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ViewModelProvider(this).get(MainViewModel::class.java).user=null

        //Registration Button to Registration Fragment
        view.findViewById<Button>(R.id.button_first).setOnClickListener {
            viewModel.navigateToSecondFragment()//Send user to registration
        }//registration button listener

        //Login Button to Customer Fragment
        view.findViewById<Button>(R.id.buttonLogin).setOnClickListener{ viewModel.login(view.findViewById<EditText>(R.id.FirstFragmentTextUserName).text.toString(),view.findViewById<EditText>(R.id.FirstFragmentTextPassword).text.toString())}//login button listener

        viewModel.navigateToSecondFragment.observe(viewLifecycleOwner, Observer<Boolean> { status ->
            if (status) {
                findNavController().navigate(R.id.action_FirstFragment_to_SecondFragment)
                viewModel.onNavigatedToSecondFragment()
            }
        })
        viewModel.navigateToJobSearcher.observe(viewLifecycleOwner, Observer<Boolean> { status ->
            if (status) {
                findNavController().navigate(R.id.action_FirstFragment_to_jobSearcher)
                viewModel.onNavigatedToJobSearcher()
            }
        })


    }//function on view created

}//class First Fragment