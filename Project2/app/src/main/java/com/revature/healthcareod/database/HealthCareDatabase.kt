package com.revature.healthcareod.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.revature.healthcareod.database.jobdatafiles.JobData
import com.revature.healthcareod.database.jobdatafiles.JobDataDao
import com.revature.healthcareod.database.nursedatafiles.NurseData
import com.revature.healthcareod.database.nursedatafiles.NurseDataDao
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


@Database(entities = [NurseData::class, JobData::class], version = 5)
abstract class HealthCareDatabase : RoomDatabase() {
    abstract fun nurseDataDao() : NurseDataDao
    abstract fun jobDataDao() : JobDataDao

    companion object {
        @Volatile
        private var INSTANCE: HealthCareDatabase? = null

        fun getDatabase(
            context: Context,
            scope: CoroutineScope
        ): HealthCareDatabase {
            // if the INSTANCE is not null, then return it,
            // if it is, then create the database
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    HealthCareDatabase::class.java,
                    "HealthCareDatabase"
                )
                    // Wipes and rebuilds instead of migrating if no Migration object.
                    // Migration is not part of this codelab.
                    .fallbackToDestructiveMigration()
                    .addCallback(HealthCareDatabaseCallback(scope))
                    .build()
                INSTANCE = instance
                // return instance
                instance
            }
        }

        private class HealthCareDatabaseCallback(
            private val scope: CoroutineScope
        ) : RoomDatabase.Callback() {
            /**
             * Override the onCreate method to populate the database.
             */
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                // If you want to keep the data through app restarts,
                // comment out the following line.
                INSTANCE?.let { database ->
                    scope.launch(Dispatchers.IO) {
                        //populateDatabase(database.wordDao())
                    }
                }
            }
        }
    }
}