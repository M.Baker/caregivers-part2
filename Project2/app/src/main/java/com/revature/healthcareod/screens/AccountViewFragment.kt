package com.revature.healthcareod.screens

import android.os.Bundle
import android.text.SpannableStringBuilder
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.revature.healthcareod.CustomLayoutManager
import com.revature.healthcareod.R
import com.revature.healthcareod.databinding.FragmentAccountViewBinding
import com.revature.healthcareod.datahandlers.EduAdapter
import com.revature.healthcareod.datahandlers.NurseAccount
import com.revature.healthcareod.viewmodels.MainViewModel

//TODO:Split into two screens
// Account view by Matt
class AccountViewFragment : Fragment() {

    private val viewModel:MainViewModel by activityViewModels()

    private var _binding: FragmentAccountViewBinding? = null
    private val binding get() = _binding!!
    lateinit var userName: String
    private var userRef: NurseAccount? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //Load the user

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentAccountViewBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = this
        val view = binding.root
        binding.viewModel=viewModel
        //binding.accViewRecV.layoutManager=CustomLayoutManager(requireContext())
        binding.accViewRecV.adapter= EduAdapter(viewModel.eduList)
//        binding.btnBack.setOnClickListener { viewModel.navigateToJobSearcher() }
        binding.btnSaveProfile.setOnClickListener { viewModel.navigateToAccountEdit()}
//        binding.btnSavePassword.setOnClickListener { viewModel.savePass(
//            binding.editTextPassword.text.toString(),
//            binding.editTextTextConfirmPassword.text.toString()
//        ) }
//        binding.btnSubmitEdu.setOnClickListener { viewModel.saveEdu(
//            binding.accViewSchoolEt.text.toString(),
//            binding.accViewGradDat.text.toString(),
//            binding.accViewLevel.text.toString(),
//            binding.accViewMajorEt.text.toString()
//        ) }
        binding.editTextAccViewFName.text=SpannableStringBuilder(viewModel.firstName)
        binding.editTextAccViewLName.text=SpannableStringBuilder(viewModel.lastName)
        binding.editTextAccViewAdrs.text=SpannableStringBuilder(viewModel.address)

        //Inflate the layout for this fragment
//        viewModel.edu.observe(viewLifecycleOwner, Observer<Boolean> { status ->
//            if (status) {
//                Log.e("EDU","test")
//                binding.accViewRecV.adapter=EduAdapter(viewModel.eduList)
//                viewModel.onEduUpdated()
//                binding.accViewMajorEt.text=viewModel.blankedit
//                binding.accViewSchoolEt.text=viewModel.blankedit
//                binding.accViewGradDat.text=viewModel.blankedit
//                binding.accViewLevel.text=viewModel.blankedit
//
//            }
//        })

        viewModel.navigateToJobSearcher.observe(viewLifecycleOwner, Observer<Boolean> { status ->
            if (status) {
                findNavController().navigate(R.id.action_accountViewFragment_to_jobSearcher)
                viewModel.onNavigatedToJobSearcher()
            }
        })
        viewModel.navigateToAccountEdit.observe(viewLifecycleOwner, Observer<Boolean> { status ->
            if (status) {
                findNavController().navigate(R.id.action_accountViewFragment_to_accountEditFragment)
                viewModel.onNavigatedToAccountEdit()
            }
        })

//        viewModel.passC.observe(viewLifecycleOwner, Observer<Int> { status ->
//            if (status != 0) {
//                Toast.makeText(this.requireContext(), when (status) {
//                    1 -> getString(R.string.success_password).also { viewModel.onPassCRead() }
//                    2 -> getString(R.string.error_message_unmatched).also { viewModel.onPassCRead() }
//                    3 -> getString(R.string.blank_password).also { viewModel.onPassCRead() }
//                    else -> "Severe Error"
//                }, Toast.LENGTH_SHORT).show()
//            }
//        })
//        viewModel.pU.observe(viewLifecycleOwner, Observer<Int> { status ->
//            if (status != 0) {
//                Toast.makeText(this.requireContext(), when (status) {
//                    1 -> getString(R.string.success_profile).also {
//                        binding.editTextAccViewFName.text =
//                            SpannableStringBuilder(viewModel.firstName)
//                        binding.editTextAccViewLName.text =
//                            SpannableStringBuilder(viewModel.lastName)
//                        binding.editTextAccViewAdrs.text = SpannableStringBuilder(viewModel.address)
//                        viewModel.onPURead()
//                    }
//                    2 -> getString(R.string.error_message_nochanges).also { viewModel.onPURead() }
//                    else -> "Severe Error"
//                }, Toast.LENGTH_SHORT).show()
//            }
//        })


        return view
    }

    // make a new item for the list of education items

    //update misc profile info

//    private fun submit() {
//        userRef?.let {
//            val fName = binding.editTextAccViewFName.text.toString().trim()
//            val lName = binding.editTextAccViewLName.text.toString().trim()
//            val adrs = binding.editTextAccViewAdrs.text.toString()
//            var scs=false
//            if (fName.trim() != "" && lName.trim() != "" && fName != it.firstName && lName != it.lastName) {
//                it.firstName = fName.trim()
//                it.lastName = lName
//                binding.editTextAccViewFName.text=SpannableStringBuilder(fName)
//                binding.editTextAccViewLName.text=SpannableStringBuilder(lName)
//                scs=true
//            }
//            if (adrs.trim() != "" && adrs != it.address) {
//                it.address = adrs
//                binding.editTextAccViewAdrs.text=SpannableStringBuilder(adrs)
//                scs=true
//            }
//            if (scs){
//                Toast.makeText(requireContext(), getString(R.string.success_profile), Toast.LENGTH_SHORT).show()
//            }else{
//                Toast.makeText(requireContext(), getString(R.string.error_message_nochanges), Toast.LENGTH_SHORT).show()
//            }
//        }
//    }

    /// change whats in passwork if it meets password requirements

//    private fun savePass() {
//        userRef?.let {
//            val p = binding.editTextPassword.text.toString()
//            val p2 = binding.editTextTextConfirmPassword.text.toString()
//            if (p != "") {
//                if (p == p2) {
//                    it.password = p
//                    Toast.makeText(requireContext(), getString(R.string.success_password), Toast.LENGTH_LONG).show()
//                }else{
//                    Toast.makeText(requireContext(), getString(R.string.error_message_unmatched), Toast.LENGTH_SHORT).show()
//                }
//            } else {
//                Toast.makeText(requireContext(), getString(R.string.blank_password), Toast.LENGTH_SHORT).show()
//            }
//        }
//
//    }


}