package com.revature.healthcareod.database.nursedatafiles

import androidx.annotation.WorkerThread
import kotlinx.coroutines.flow.Flow

class NurseDataRepository(private val nurseDataDao: NurseDataDao){

    val allNursesData: Flow<List<NurseData>> = nurseDataDao.getAllNursesData()


    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun update(nurseData: NurseData) {
        nurseDataDao.updateNurseData(nurseData)
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(nurseData: NurseData) {
        nurseDataDao.insert(nurseData)
    }
}

