package com.revature.healthcareod.datahandlers

import android.content.Context
import android.content.res.Resources
import android.provider.Settings.Global.getString
import android.provider.Settings.Secure.getString
import android.provider.Settings.System.getString
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.revature.healthcareod.R
import com.revature.healthcareod.database.jobdatafiles.JobData
import com.revature.healthcareod.databinding.CurrentJobsElementBinding
import com.revature.healthcareod.databinding.JobElementBinding
import com.revature.healthcareod.databinding.JobFinderElementBinding
import com.revature.healthcareod.viewmodels.MainViewModel

class CurrentJobsAdapter(private val dataset: ArrayList<Job>,val viewModel: MainViewModel):
    RecyclerView.Adapter<CurrentJobsAdapter.ItemViewHolder>()  {

    class ItemViewHolder(private var binding: CurrentJobsElementBinding,):RecyclerView.ViewHolder(binding.root){
        fun bind(job: Job,viewModel: MainViewModel){
            binding.job=job
            binding.testinclude.job=job
            binding.viewModel=viewModel
            binding.locationButton.text = job.textAddress
            binding.locationButton.setOnClickListener {
                job.address?.let { it1 -> viewModel.openMap(it1) }
            }
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        println("made it into the job finder")
        val adapterLayout= LayoutInflater.from(parent.context).inflate(R.layout.job_finder_element,parent,false)
        return ItemViewHolder(CurrentJobsElementBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val job=dataset[position]
        holder.bind(job,viewModel)
//        holder.declineButton.text = "Decline"
//        holder.h.text=DummyData.getHosNameBId(job.hospitalId)
//        holder.jn.text=job.jobName
//        holder.d.text=job.jobDescription
//        holder.r.text=job.ratePerHour.toString()
//        holder.t.text=job.totalAmount.toString()
//        holder.declineButton.setOnClickListener{
//            dataset[position].userName = " "
//            dataset[position].status = "Open"
//            updateJobDataFunction.invoke(JobData(
//                    dataset[position].jobId,
//                    dataset[position].hospitalId,
//                    dataset[position].userName,
//                    dataset[position].ratePerHour,
//                    dataset[position].totalAmount,
//                    dataset[position].jobName?:"null",
//                    dataset[position].jobDescription?:"null",
//                    dataset[position].startDate?:"null",
//                    dataset[position].endDate?:"null",
//                    dataset[position].status?:"null"))
//            dataset.remove(dataset[position])
//            notifyDataSetChanged()
//        }

    }

    override fun getItemCount(): Int {
        return dataset.size
    }

}