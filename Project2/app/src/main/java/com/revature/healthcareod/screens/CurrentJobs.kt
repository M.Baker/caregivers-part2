package com.revature.healthcareod.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.gms.maps.model.LatLng
import com.revature.healthcareod.R
import com.revature.healthcareod.databinding.FragmentCurrentJobsBinding
import com.revature.healthcareod.datahandlers.CurrentJobsAdapter
import com.revature.healthcareod.datahandlers.NurseAccount
import com.revature.healthcareod.viewmodels.MainViewModel

class CurrentJobs : Fragment() {

    private val viewModel:MainViewModel by activityViewModels()

    private var _binding: FragmentCurrentJobsBinding?=null
    private val binding get()=_binding!!
    private var userRef: NurseAccount? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        println("The view is being created")
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        _binding= FragmentCurrentJobsBinding.inflate(inflater,container,false)
        val view=binding.root
        binding.rvCurrentJobs.adapter= CurrentJobsAdapter(viewModel.allCurrentJobs , viewModel)
//        binding.btnFjBack.setOnClickListener { viewModel.navigateToJobSearcher() }
        viewModel.navigateToJobSearcher.observe(viewLifecycleOwner, Observer<Boolean> { status ->
            if (status) {
                findNavController().navigate(R.id.action_currentJobs_to_jobSearcher)
                viewModel.onNavigatedToJobSearcher()
            }
        })
        viewModel.currentJobsUpdated.observe(viewLifecycleOwner,Observer<Boolean>{status->
            if(status) {
                binding.rvCurrentJobs.adapter =
                    CurrentJobsAdapter(viewModel.allCurrentJobs, viewModel)
                viewModel.onCurrentJobsUpdated()
            }
        })

        viewModel.locationChanged.observe(viewLifecycleOwner,Observer<Boolean>{ status->
            if(status) {
                findNavController().navigate(R.id.action_currentJobs_to_maps)
                viewModel.closeMap()
            }
        })


        // binding.btnBack.setOnClickListener { back() }
        //binding.btnSaveProfile.setOnClickListener { submit() }
        //Inflate the layout for this fragment
        return view
    }

}